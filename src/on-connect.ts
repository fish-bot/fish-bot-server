import * as Debug from "debug";
import { Socket } from "socket.io";
import { randomBytes } from "crypto";
import { promisify } from "util";
import { Store } from "./services/store";
import { ISession } from "./services/session";
import { IMongo } from "./adapter/mongo";
import { deviceMapper, deviceControlPropertiesMapper } from "./mappers";
import { Message, OnlineDevice, Activity } from "./types";
import { findDeviceControlById } from "./helpers/find-device-control-by-id";
import { parseDevice, parseDeviceControlProperties } from "./parsers";

const debug = Debug("app:on-connect");

export function OnConnect(store: Store, session: ISession, mongo: IMongo) {

  return async function (socket: Socket) {
    debug(`new connection, id=%s`, socket.id);

    store.addSocket(socket);

    socket.on("message", async function (message) {
      debug("incoming message; action=%s", message.action);

      try {

        if (message.action === "login") {
          const payload = message.payload || {};
          const { passphrase, api_key: apiKey, access_token: accessToken } = payload;

          if (passphrase || accessToken) {
            let newAccessToken = accessToken;

            if (accessToken) {
              if (!(await session.verifyAccessToken(accessToken))) {
                socket.emit("message", {
                  type: "response",
                  action: "login",
                  error: "Invalid access token"
                } as Message);

                // disconnect
                return socket.disconnect();
              }
            }
            else {
              newAccessToken = await session.login(passphrase);

              if (!newAccessToken) {
                socket.emit("message", {
                  type: "response",
                  action: "login",
                  error: "Invalid passphrase"
                } as Message);

                // disconnect
                return socket.disconnect();
              }
            }

            store.addUser({
              id: randomBytes(4).toString("hex"),
              socket: socket
            });

            await insertActivity({
              name: `User logged in`,
              group: socket.id,
              date: new Date()
            })

            return socket.emit("message", {
              type: "response",
              action: "login",
              payload: {
                accessToken: newAccessToken,
                devices: store.getOnlineDevices().map(deviceMapper)
              }
            } as Message);
          }

          if (apiKey) {
            if (!(await session.verifyApiKey(apiKey))) {
              socket.emit("message", {
                type: "response",
                action: "login",
                error: "Invalid api key"
              } as Message);

              // disconnect
              return socket.disconnect();
            }

            const device = parseDevice(payload.device);
            const deviceOnline = Object.assign({}, device, {
              socket: socket
            }) as OnlineDevice;

            store.addOnlineDevice(deviceOnline)

            socket.emit("message", {
              type: "response",
              action: "login",
            } as Message)

            await insertActivity({
              name: `Device logged in`,
              group: socket.id,
              date: new Date()
            });

            return store.getUsers().forEach(user => {
              user.socket.emit("message", {
                type: "push",
                action: "device_online",
                payload: {
                  device: deviceMapper(device)
                }
              } as Message)
            })
          }

          return socket.emit("message", {
            type: "response",
            action: "login",
            error: "Invalid payload"
          } as Message);
        }

        if (message.action === "update_device_control_properties") {
          const payload = message.payload || {};
          const { device_id: deviceId, control_id: controlId } = payload;
          const device = store.getOnlineDeviceById(deviceId);

          if (!device) {
            return socket.emit("message", {
              type: "response",
              action: "update_device_control_properties",
              error: "Device doesn't exist"
            } as Message);
          }

          const control = findDeviceControlById(device, controlId);

          if (!control) {
            return socket.emit("message", {
              type: "response",
              action: "update_device_control_properties",
              error: "Control doesn't exist"
            } as Message);
          }

          const propertiesToChange = parseDeviceControlProperties(payload.properties)

          if (Object.keys(propertiesToChange).length === 0) {
            return socket.emit("message", {
              type: "response",
              action: "update_device_control_properties",
              error: "No properties to be changed"
            } as Message);
          }

          const updatedControl = Object.assign({}, control, {
            properties: Object.assign({}, control.properties, propertiesToChange)
          })

          store.updateOnlineDevice(deviceId, {
            controls: device.controls.map(v => v.id === controlId ? updatedControl : v)
          })

          const pushMessage = {
            type: "push",
            action: "updated_device_control_properties",
            payload: {
              device_id: device.id,
              control_id: control.id,
              properties: deviceControlPropertiesMapper(propertiesToChange)
            }
          } as Message

          store.getUsers().forEach(user => {
            user.socket.emit("message", pushMessage)
          });

          device.socket.emit("message", pushMessage);

          insertActivity({
            name: "Update device control",
            group: socket.id,
            properties: {
              device: {
                id: device.id
              },
              control: {
                id: control.id
              },
              changed: propertiesToChange
            },
            date: new Date()
          })

          return;
        }

        if (message.action === "get_activities") {
          const collection = await mongo.activities();

          try {
            const find = collection.find({}).limit(200).sort({ date: -1 });
            const activities = await promisify(find.toArray).bind(find)() as Activity[];

            socket.emit("message", {
              type: "response",
              action: "get_activities",
              payload: {
                activities
              }
            } as Message)
          }
          catch(err) {
            socket.emit("message", {
              type: "response",
              action: "get_activities",
              error: JSON.stringify(err)
            } as Message)
          }

          return;
        }

        debug("invalid request, action=%s", message.action)
      }
      catch (e) {
        socket.emit("message", {
          type: "response",
          action: "internal_error",
          error: JSON.stringify(e)
        } as Message);

        // disconnect
        return socket.disconnect();
      }
    });

    socket.on("disconnect", (reason) => {
      debug("socket disconnected, id=%s, reason=%s", socket.id, reason)

      const device = store.getOnlineDeviceBySocketId(socket.id);

      store.removeSocket(socket.id);
      store.removeUserBySocketId(socket.id);

      if (device) {
        store.removeOnlineDeviceById(device.id)
        store.getUsers().forEach(user => {
          user.socket.emit("message", {
            type: "push",
            action: "device_offline",
            payload: {
              device_id: device.id
            }
          } as Message)
        })
      }

      insertActivity({
        name: `${device ? "Device" : "User"} disconnected`,
        group: socket.id,
        date: new Date()
      });
    });
  }

  async function insertActivity(activity: Activity): Promise<boolean> {
    try {
      const activities = await mongo.activities();

      await activities.insertOne(activity);

      return true;
    }
    catch(error) {
      debug("add activity failed: %O", error)

      return false
    }    
  }
}