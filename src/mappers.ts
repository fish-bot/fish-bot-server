import { Device, DeviceViewModel, DeviceControl, DeviceControlViewModel, DeviceControlTypes, DeviceControlProperties, DeviceControlPropertiesViewModel } from "./types";

export function deviceMapper(device: Device): DeviceViewModel {
  return {
    id: device.id,
    name: device.name,
    controls: device.controls.map(deviceControlMapper)
  }
}

export function deviceControlMapper(deviceControl: DeviceControl): DeviceControlViewModel {
  if (deviceControl.type === DeviceControlTypes.Relay) {
    const properties = (deviceControl as DeviceControl).properties;

    return {
      id: deviceControl.id,
      name: deviceControl.name,
      type: DeviceControlTypes[deviceControl.type],
      properties: deviceControlPropertiesMapper(properties)
    }
  }
}

export function deviceControlPropertiesMapper(deviceControlProperties: DeviceControlProperties): DeviceControlPropertiesViewModel {
  const properties: Partial<DeviceControlPropertiesViewModel> = {}

  if (deviceControlProperties.turnedOn != null) {
    properties.turned_on = deviceControlProperties.turnedOn;
  }
  
  if (deviceControlProperties.synced != null) {
    properties.synced = deviceControlProperties.synced;
  }

  return properties;
}