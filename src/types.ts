import { Socket } from "socket.io";

export interface User {
  id: string;
  socket: Socket
}

export interface Device {
  id: string;
  name: string;
  controls: DeviceControl[];
}

export interface OnlineDevice extends Device {
  socket: Socket;
}

export interface DeviceViewModel {
  id: string;
  name: string;
  controls: DeviceControlViewModel[];
}

export enum DeviceControlTypes {
  Relay = 'relay'
}

export interface DeviceControlViewModel {
  id: string;
  name: string;
  type: string;
  properties: object;
}

export interface DeviceControl {
  id: string;
  name: string;
  type: DeviceControlTypes;
  properties: DeviceControlProperties;
}

export interface DeviceControlProperties {
  turnedOn?: boolean | number;
  synced?: boolean;
}

export interface DeviceControlPropertiesViewModel {
  turned_on?: boolean | number;
  synced?: boolean | number;
}

export interface Message {
  type: "push" | "response";
  action: string;
  payload?: object;
  error?: string;
}

export interface Activity {
  name: string;
  group?: string;
  properties?: {
    [param: string]: object
  }
  date: Date;
};
