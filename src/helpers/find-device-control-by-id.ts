import { Device, DeviceControl } from "../types";

export function findDeviceControlById(device: Device, controlId: string): DeviceControl {
  return device && (device.controls || []).find(control => control.id === controlId);
}
