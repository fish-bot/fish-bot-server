import * as Koa from "koa";
import * as koaStatic from "koa-static";
import * as SocketIO from 'socket.io';
import { createServer } from "http";
import { readFileSync } from "fs";
import { resolve, join } from "path";
import * as Debug from "debug";
import { config } from "./config"
import { Store } from "./services/store";
import { Session } from "./services/session";
import { Redis } from "./adapter/redis";
import { Mongo } from "./adapter/mongo";
import { OnConnect } from "./on-connect";

const debug = Debug("app");
const app = new Koa();
const redisStore = Redis(config.REDIS_URL);
const session = Session(redisStore);
const store = new Store();
const mongo = Mongo(config.MONGODB_URI);

app.use(koaStatic(join(__dirname, '../public')))
app.use(async (ctx) => {
  ctx.type = 'html'
  ctx.body = readFileSync(resolve(__dirname, "../views/index.html"));
})

const server = createServer(app.callback())
const io = SocketIO(server, {
  pingTimeout: 15000,
  pingInterval: 30000,
})

io.on('connection', OnConnect(store, session, mongo));

server.listen(config.PORT, () => console.log(`server is started on port ${config.PORT}`))
