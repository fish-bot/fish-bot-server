import * as argon2 from "argon2";
import * as Debug from "debug";
import { config } from "../config";
import { randomBytes } from "crypto";

const debug = Debug("app:session");
const API_KEYS = config.API_KEYS.split(';').map(v => (v || "").trim())
const PASSPHRASES = config.PASSPHRASES.split(';').map(v => (v || "").trim())
const salt = Buffer.alloc(16, config.SALT);

export interface ISessionStorage {
  get(key: string): Promise<string>;
  set(key: string, value: string): Promise<"OK">;
}

export interface ISession {
  login(passphrase: string): Promise<string>;
  verifyApiKey(apiKey: string): Promise<boolean>;
  verifyAccessToken(accessToken: string): Promise<boolean>;
}

export function Session(storage: ISessionStorage): ISession {

  async function login(passphrase: string): Promise<string> {
    for (let i = 0; i < PASSPHRASES.length; i++) {
      if (await argon2.verify(PASSPHRASES[i], passphrase)) {
        return await generateAccessToken();
      }
    }
  }

  async function verifyApiKey(apiKey: string): Promise<boolean> {
    for (let i = 0; i < API_KEYS.length; i++) {
      if (await argon2.verify(API_KEYS[i], apiKey)) {
        return true
      }
    }

    return false;
  }

  async function verifyAccessToken(accessToken: string): Promise<boolean> {
    const sessionToken = await argon2.hash(accessToken, { salt });
    const session = await storage.get(`session-${sessionToken}`);

    return !!session;
  }

  async function generateAccessToken(): Promise<string> {
    const accessToken = randomBytes(128).toString("hex");
    const sessionToken = await argon2.hash(accessToken, { salt });

    await storage.set(`session-${sessionToken}`, "1");

    return accessToken;
  }

  return {
    login,
    verifyApiKey,
    verifyAccessToken
  }
}