import { User, Device, OnlineDevice } from "../types";
import { Socket } from "socket.io";

export class Store {

  onlineDevisesById: {
    [key: string]: OnlineDevice
  } = {};

  onlineDevisesBySocketId: {
    [key: string]: OnlineDevice
  } = {};

  users: User[] = [];
  sockets: Socket[] = [];

  addUser(user: User) {
    return this.users.push(user), this;
  }

  getUsers() {
    return this.users;
  }

  getUserBySocketId(socketId: string) {
    return this.users.find(user => user.socket.id === socketId);
  }

  removeUserBySocketId(socketId: string) {
    return this.users = this.users.filter(v => v.socket.id !== socketId),
      this;
  }

  addOnlineDevice(device: OnlineDevice): this {
    return (this.onlineDevisesById[device.id] = device),
      (this.onlineDevisesBySocketId[device.socket.id] = device),
      this;
  }

  getOnlineDevices(): OnlineDevice[] {
    return Object.values(this.onlineDevisesById);
  }

  getOnlineDeviceById(deviceId: string) {
    return this.onlineDevisesById[deviceId];
  }

  getOnlineDeviceBySocketId(socketId: string) {
    return this.onlineDevisesBySocketId[socketId];
  }

  updateOnlineDevice(deviceId, device: Partial<Device>): this {
    const changedDevice = Object.assign({}, this.onlineDevisesById[deviceId], device)
    return (this.onlineDevisesById[deviceId] = changedDevice),
      (this.onlineDevisesBySocketId[changedDevice.socket.id] = changedDevice),
      this
  }

  removeOnlineDeviceById(deviceId: string) {
    const device = this.onlineDevisesById[deviceId];

    if (device) {
      delete this.onlineDevisesById[device.id];
      delete this.onlineDevisesBySocketId[device.socket.id];
    }

    return this;
  }

  removeUserById(id: string) {
    return (this.users = this.users.filter(v => v.id !== id)), this;
  }

  addSocket(socket: Socket) {
    return this.sockets.push(socket), this;
  }

  removeSocket(socketId: string) {
    return this.sockets = this.sockets.filter(v => v.id !== socketId), this;
  }
}
