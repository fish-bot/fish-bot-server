import * as dotenv from "dotenv";

interface IConfig {
  SALT: string;
  PORT: number | string;
  API_KEYS: string;
  PASSPHRASES: string;
  REDIS_URL: string;
  MONGODB_URI: string;
}

const result = dotenv.config();

if (result.error) {
  console.error("dotenv.config error: ", result.error);
}

if (!process.env.SALT) {
  console.error(new Error("process.env.SALT is required"));
}

if (!process.env.PORT) {
  console.log("process.env.PORT is not specified. Default is 3000");
}

if (!process.env.REDIS_URL) {
  console.error(new Error("process.env.REDIS_URL is required"));
}

if (!process.env.MONGODB_URI) {
  console.error(new Error("process.env.MONGODB_URI is required"));
}

if (!process.env.API_KEYS) {
  console.error(new Error("process.env.API_KEYS is required"));
}

if (!process.env.PASSPHRASES) {
  console.error(new Error("process.env.PASSPHRASES is required"));
}

export const config: IConfig = {
  SALT: process.env.SALT,
  PORT: process.env.PORT || 3000,
  API_KEYS: process.env.API_KEYS,
  PASSPHRASES: process.env.PASSPHRASES,
  REDIS_URL: process.env.REDIS_URL,
  MONGODB_URI: process.env.MONGODB_URI,
};
