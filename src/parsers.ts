import { DeviceControl, DeviceControlProperties, DeviceControlTypes, Device } from "./types";

export function parseDevice(payload: any = {}): Device {
  return {
    id: payload.id + '',
    name: payload.name + '',
    controls: Array.isArray(payload.controls) ? payload.controls.map(parseDeviceControl) : []
  }
}


export function parseDeviceControl(payload: any = {}): DeviceControl {
  return {
    id: (payload.id + ''),
    name: payload.name + '',
    type: parseDeviceControlType(payload.type),
    properties: parseDeviceControlProperties(payload.properties)
  }
}

export function parseDeviceControlType(type: any): DeviceControlTypes {
  if (type === DeviceControlTypes.Relay) {
    return DeviceControlTypes.Relay
  }

  throw new Error("Invalid device type provided");
}


export function parseDeviceControlProperties(payload: any = {}): DeviceControlProperties {
  const properties: Partial<DeviceControlProperties> = {};

  if (payload.turned_on != null) {
    properties.turnedOn = Boolean(payload.turned_on)
  }
  
  if (payload.synced != null) {
    properties.synced = Boolean(payload.synced)
  }

  return properties;
}