import * as redis from "redis";
import * as retry from "retry";
import * as Debug from "debug";

const debug = Debug("app:adapters:redis");

export function Redis(REDIS_URL) {
  let client = redis.createClient(REDIS_URL);

  client.on("ready", () => debug("redis is ready"));
  client.on("connect", () => debug("redis is connect"));
  client.on("reconnecting", () => debug("redis is reconnecting"));
  client.on("end", () => debug("end event"));
  client.on("error", err => debug("onerror %O", err));
  client.on("warning", (warn) => debug("redis has warning(%s)", warn));

  let promise: Promise<string> = Promise.resolve("");

  function get(key: string) {
    const operation = retry.operation({
      retries: 3,
      minTimeout: 10,
      maxTimeout: 1000
    });

    return promise = promise.then((): Promise<string> => {
      return new Promise((resolve, reject) => {
        operation.attempt(function(currentAttempt) {
          debug("attempt n. %s to GET value", currentAttempt);

          client.get(key, function(error, replay) {
            if (operation.retry(error)) {
              return;
            }

            error ? reject(operation.mainError()) : resolve(replay);
          });
        });
      });
    });
  }

  function set(key: string, value: string): Promise<"OK"> {
    const operation = retry.operation({
      retries: 3,
      minTimeout: 10,
      maxTimeout: 1000
    })

    return promise = promise.then((): Promise<"OK"> => {
      return new Promise((resolve, reject) => {
        operation.attempt(function(currentAttempt) {
          debug("attempt n. %s to SET value", currentAttempt);

          client.set(key, value, function(error, replay) {
            if (operation.retry(error)) {
              return;
            }

            error ? reject(operation.mainError()) : resolve(replay);
          });
        });
      });
    });
  }

  return {
    get,
    set
  }
}
