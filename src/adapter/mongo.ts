import { MongoClient, Db, Collection } from "mongodb";
import * as Debug from "debug";
import { promisify } from "util";
import { Activity } from "../types";

const debug = Debug("app:adapter:mongo");

export interface IMongo {
  db(): Promise<Db>;
  collection(collectionName);
  activities(): Promise<Collection<Activity>>;
  close();
}

export function Mongo(MONGODB_URI): IMongo {
  const connect: Promise<MongoClient> = new Promise((resolve, reject) => {
    MongoClient.connect(MONGODB_URI, {
      useNewUrlParser: true
    }, (error, mongoClient) => error ? reject(error) : resolve(mongoClient));
  })
  connect
    .then(() => debug("connected to mongo server"))
    .catch(error => debug("error connect to mongo %O", error));
  
  async function db(): Promise<Db> {
    return connect.then(client => client.db());
  }

  async function collection(collectionName) {
    return db().then(db => db.collection(collectionName))
  }

  async function activities(): Promise<Collection<Activity>> {
    return collection("activities");
  }

  async function close() {
    return connect.then(client => client.close())
  }

  return {
    db,
    collection,
    activities,
    close,
  }
}