/**
 * 
 * @param {object} config
 */
function Page(config) {

  /** @type {object[]} */
  var devices = [];

  /** @type {Socket} */
  var socket = null;

  /**
   * @type {Array<HTMLDivElement>}
   */
  var carts = [
    document.getElementById('loader-cart'),
    document.getElementById('login-cart'),
    document.getElementById('devices-carts'),
    document.getElementById('activities'),
  ];

  function showCart(cartName) {
    for (let i = 0; i < carts.length; i++) {
      carts[i].style.display = carts[i].id === cartName ? 'block' : 'none';
    }
  }

  function showLoginCart() {
    return showCart('login-cart');
  }

  function showDevicesCarts() {
    return showCart('devices-carts');
  }

  function showDevicesActivities() {
    return showCart('activities');
  }
  
  function showLoader() {
    return showCart('loader-cart');
  }

  function onLoginFormSubmit(event) {

    /** @type {HTMLFormElement} */
    var form = event.target;

    event.preventDefault();

    connectToSocket({
      passphrase: form.passphrase.value
    })
  }

  function onLoginByAccessToken(accessToken) {
    connectToSocket({
      access_token: accessToken
    });
  }

  function connectToSocket(connectPayload) {
    socket = io(window.location.toString(), {
      transports: ['websocket', 'polling']
    })

    socket.on('connect', onSocketConnect(connectPayload));
    socket.on('message', onSocketMessage);
    socket.on('disconnect', onSocketDisconnect);
  }

  function onSocketConnect(payload) {
    return function() {
      socket.emit("message", {
        action: "login",
        payload: payload
      });
    }
  }

  function onSocketMessage(data) {
    if (data.action === 'login') {
      return data.error ? handleLoginError(data.error) : handleLoginResponse(data.payload);
    }

    if (data.action === 'device_online') {
      return handleDeviceOnlinePush(data.payload);
    }

    if (data.action === 'device_offline') {
      return handleDeviceOfflinePush(data.payload);
    }

    if (data.action === 'updated_device_control_properties') {
      return handleUpdatedDeviceControlProperties(data.payload);
    }

    return console.log('unknown message', data);
  }

  function onSocketDisconnect() {
    console.log('disconnect');
  }

  function handleLoginResponse(payload) {
    var accessToken = payload.accessToken || "";
    devices = payload && payload.devices || [];

    localStorage.setItem("accessToken", accessToken);
    renderDevicesCarts(devices);
    showDevicesCarts();
  }

  function handleLoginError(loginError) {
    showLoginCart();
    alert(loginError);
  }

  function handleDeviceOnlinePush(payload) {
    var deviceOnline = payload.device || {};
    var device = devices.find(function(v) {
      return v.id === deviceOnline.id;
    });

    if (!device) {
      devices.unshift(deviceOnline);
    }
    else {
      Object.assign(device, deviceOnline)
    }

    renderDevicesCarts(devices);

    var onlineEl = document.getElementById("device-status-" + deviceOnline.id);
    var fieldsetEl = document.getElementById("device-fieldset-" + deviceOnline.id);

    if (onlineEl) {
      onlineEl.classList.add('device-status__online');
    }

    if (fieldsetEl) {
      fieldsetEl.disabled = false;
    }
  }

  function handleDeviceOfflinePush(payload) {
    var deviceId = payload.device_id;
    var onlineEl = document.getElementById("device-status-" + deviceId);
    var fieldsetEl = document.getElementById("device-fieldset-" + deviceId);

    if (onlineEl) {
      onlineEl.classList.remove('device-status__online');
    }

    if (fieldsetEl) {
      fieldsetEl.disabled = true;
    }
  }

  function handleUpdatedDeviceControlProperties(payload) {
    var deviceId = payload.device_id;
    var controlId = payload.control_id
    var properties = payload.properties || {};
    var controlSynced = document.getElementById(`control-synced-${deviceId}-${controlId}`);
    var controlTurnedOn = document.getElementById(`control-turned-on-${deviceId}-${controlId}`);

    if (controlSynced && properties.synced != null) {
      controlSynced.classList[
        properties.synced ? 'add' : 'remove'
      ]('hide');

      controlTurnedOn.disabled = !properties.synced;
    }

    if (controlTurnedOn && properties.turned_on != null) {
      controlTurnedOn.checked = properties.turned_on;
    }
  }

  function renderDevicesCarts(devices) {
    var container = document.getElementById('devices-carts');
    var devicesLength = devices && devices.length || 0;
    var deviceCard = null;
    var device = null;

    if (!container) {
      throw new Error('No container for device cards')
    }

    // reset content
    container.innerHTML = '';

    if (devicesLength === 0) {
      deviceCard = document.createElement('div')
      deviceCard.className = "cart";
      deviceCard.innerHTML = `<div class="cart__body">No Devices</div>`
      container.appendChild(deviceCard);
    }

    for (let i = 0; i < devicesLength; i++) {
      device = devices[i];
      deviceCard = document.createElement('div');
      deviceCard.className = "cart";
      deviceCard.id = "device-cart-" + device.id;
      deviceCard.innerHTML = `<div class="cart__head">
        <span class="device-name">${device.name}</span>
        <span class="device-status device-status__online" id="device-status-${device.id}"></span>
      </div>
      <div class="list">
        <fieldset id="device-fieldset-${device.id}" class="list-fieldset">
          ${(device.controls || []).map(function(control) {
            return `<div class="list-item">
              <div class="list-item__text">${control.name}</div>
              <div class="list-item__controls">
                <span class="list-item__loader hide" id="control-synced-${device.id}-${control.id}">
                  <span class="loader">
                    <span class="loader__circle"></span>
                  </span>
                </span>

                <label class="toggle">
                  <input type="checkbox" id="control-turned-on-${device.id}-${control.id}" onchange="page.onToggleDeviceControl(event, '${device.id}', '${control.id}')" ${control.properties.turned_on ? 'checked=""' : ''}>
                  <div class="toggle__slide"></div>
                </label>
              </div>
            </div><!-- /.list-item -->`
          }).join('')}
        </fieldset>
      </div>
      `
      container.appendChild(deviceCard);
    }

    // Add link with activities
    var footer = document.createElement('div');

    footer.innerHTML = ['<div class="footer">',
      '<a href="#" onclick="return page.loadDeviceActivities()">Activities</a>',
    '</div>'].join('');

    container.appendChild(footer);
  }


  function renderActivities(activities) {
    var container = document.getElementById('activities');
    var card = null;

    activities = activities || []

    if (!container) {
      throw new Error('No container for activities')
    }

    // reset content
    container.innerHTML = '';

    card = document.createElement('div')
    card.className = "cart";

    if (activities.length === 0) {  
      card.innerHTML = `<div class="cart__body">No Activities</div>`
    }
    else {
      activities = activities.map(function (activity) {
        return ['<div class="activity-item">',
          '<span class="activity-item__name">',
            (activity.name || ''),
          '</span>',

          '<span class="activity-item__properties">',
            (activity.properties ? JSON.stringify(activity.properties) : ''),
          '</span>',

          '<span class="activity-item__date" title="' + (activity.date || '') +'">',
            (activity.date ? timeSince(new Date(activity.date)) : "recently"),
          '</span>',
        ].join('\n')
      }).join('\n');

      card.innerHTML = [
        '<div class="cart__head">',
          'Activity',
        '</div>',
        '<div class="cart__body">',
          '<div class="activities-list">',
            activities,
          '</div>',
        '</div>'
      ].join('')
    }

    container.appendChild(card);

    // Add link to devices
    var footer = document.createElement('div');

    footer.innerHTML = ['<div class="footer">',
      '<button class="btn" onclick="return page.showDevicesCarts()">Devices</button>',
    '</div>'].join('');

    container.appendChild(footer);
  }

  function renderError(errorText, targetId) {
    var container = document.getElementById(targetId);
    var card = null;

    if (!container) {
      throw new Error('No container for showing error message')
    }

    // reset content
    container.innerHTML = '';

    card = document.createElement('div')
    card.className = "cart";
    card.innerHTML = [
      '<div class="cart__head">Error</div>',
      '<div class="cart__body">',
        errorText,
      '</div>'
    ].join('')
    container.appendChild(card);
    return;
  }

  function onToggleDeviceControl(event, deviceId, controlId) {
    var turnedOn = event.target.checked;
    var controlSynced = document.getElementById(`control-synced-${deviceId}-${controlId}`);
    var controlTurnedOn = document.getElementById(`control-turned-on-${deviceId}-${controlId}`);

    if (controlSynced) {
      controlSynced.classList.remove('hide');
    }

    if (controlTurnedOn) {
      controlTurnedOn.disabled = true;
    }

    socket.emit('message', {
      action: 'update_device_control_properties',
      payload: {
        device_id: deviceId,
        control_id: controlId,
        properties: {
          turned_on: turnedOn,
          synced: false
        }
      }
    })
  }

  function loadDeviceActivities() {
    let timeout;

    start();

    function start() {
      showLoader();
      setTimer();
      socket.on("message", onGetActivitiesResponse);
      socket.emit("message", {
        action: "get_activities"
      })
    }
    
    function stopListeners() {
      socket.off("message", onGetActivitiesResponse);
      clearTimeout(timeout);
    }

    function setTimer() {
      timeout = setTimeout(function() {
        stopListeners();
        renderError("Timeout error", "activities");
        showDevicesActivities();
      }, 5000);
    }

    function onGetActivitiesResponse(data) {
      if (data.action !== "get_activities") {
        return;
      }

      stopListeners()

      if (data.error) {
        renderError(data.error, "activities");
      }
      else {
        renderActivities(data.payload && data.payload.activities);
      }

      showDevicesActivities();
    }

    return false;
  }

  function timeSince(date) {
    var seconds = Math.floor((new Date() - date) / 1000);
    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
      return interval + " years";
    }

    interval = Math.floor(seconds / 2592000);

    if (interval > 1) {
      return interval + " months";
    }

    interval = Math.floor(seconds / 86400);

    if (interval > 1) {
      return interval + " days";
    }

    interval = Math.floor(seconds / 3600);

    if (interval > 1) {
      return interval + " hours";
    }

    interval = Math.floor(seconds / 60);

    if (interval > 1) {
      return interval + " minutes";
    }

    return Math.floor(seconds) + " seconds";
  }

  return {
    showLoginCart: showLoginCart,
    showDevicesCarts: showDevicesCarts,
    onLoginFormSubmit: onLoginFormSubmit,
    onLoginByAccessToken: onLoginByAccessToken,
    onToggleDeviceControl: onToggleDeviceControl,
    loadDeviceActivities: loadDeviceActivities
  }
}
